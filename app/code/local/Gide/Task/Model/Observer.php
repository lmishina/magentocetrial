<?php

class Gide_Task_Model_Observer
{
    public function insertSnippet(Varien_Event_Observer $observer)
    {
        $layout = Mage::getSingleton('core/layout');
        $content = $layout->getBlock('content');
        $newBlock = $layout->createBlock('task/snippet', 'snippet');
        $newBlock->setTemplate('task/snippet.phtml');
        $content->append($newBlock);
        return $this;
    }
}
