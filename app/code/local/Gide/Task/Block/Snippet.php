<?php

class Gide_Task_Block_Snippet extends Mage_Core_Block_Template
{

    public function getProduct()
    {
        $product = Mage::registry('current_product');
        return ($product && $product->getEntityId()) ? $product : false;
    }

    public function getStructuredData()
    {
        // get product
        $product = $this->getProduct();

        // check if $product exists
        if ($product) {
            $productId = $product->getEntityId();
            $storeId = Mage::app()->getStore()->getId();
            $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
            $productCondition = 'http://schema.org/';

            // Not default Magento attribute
            switch ($product->getData('condition')) {
                case('damaged'):
                    $productCondition .= 'DamagedCondition';
                    break;
                case('refurbished'):
                    $productCondition .= 'RefurbishedCondition';
                    break;
                case('used'):
                    $productCondition .= 'UsedCondition';
                    break;
                default:
                    $productCondition .= 'NewCondition';
            }

            $info = array(
                'availability' => $product->isAvailable() ? 'http://schema.org/InStock' : 'http://schema.org/OutOfStock',
                'condition' => $productCondition
            );

            // Reviews
            $reviewSummary = Mage::getModel('review/review/summary');
            $ratingData = Mage::getModel('review/review_summary')->setStoreId($storeId)->load($productId);

            $reviews = Mage::getModel('review/review')
                ->getCollection()
                ->addStoreFilter($storeId)
                ->addStatusFilter(1)
                ->addFieldToFilter('entity_id', 1)
                ->addFieldToFilter('entity_pk_value', $productId)
                ->setDateOrder()
                ->addRateVotes()
                ->getItems();

            if (count($reviews) > 0) {
                foreach ($reviews as $r) {
                    $ratings = array();
                    foreach ($r->getRatingVotes() as $vote) {
                        $ratings[] = $vote->getPercent();
                    }

                    $avgdata = array_sum($ratings) / count($ratings);
                    $avgdata = number_format(floor(($avgdata / 20) * 2) / 2, 1); // average rating (1-5 range)
                    $avg[] = array(
                        '@type' => 'Rating',
                        'ratingValue' => $avgdata
                    );
                }
            }

            $info['reviewCount'] = $reviewSummary->getTotalReviews($product->getId(), true);
            $info['ratingValue'] = number_format(floor(($ratingData['rating_summary'] / 20) * 2) / 2, 1); // average rating (1-5 range)

            if ($product->getShortDescription()) {
                $info['description'] = html_entity_decode(strip_tags($product->getShortDescription()));
            } else {
                $info['description'] = Mage::helper('core/string')->substr(html_entity_decode(strip_tags($product->getDescription())), 0, 165);
            }

            // Offers
            $offer = array(
                '@type' => '',
                'priceCurrency' => $currencyCode,
                'itemCondition' => $info['condition'],
                'availability' => $info['availability']
            );

            if ((float)$product->getSpecialPrice() > 0 && $product->getSpecialToDate()) {
                $offer['priceValidUntil'] = Mage::getModel('core/date')->date('Y-m-d', $product->getSpecialToDate());
            }

            if ($product->getTypeId() == "bundle" || $product->getTypeId() == "grouped") {
                $priceModel = $product->getPriceModel();

                if ($product->getTypeId() == "bundle") {
                    list($minimalPrice, $maximalPrice) = $priceModel->getTotalPrices($product, null, null, false);
                } else {
                    $maximalPrice = 0;
                    $minimalPrice = null;
                    $gProducts = $product->getTypeInstance(true)->getAssociatedProducts($product);

                    foreach ($gProducts as $gProduct) {
                        $gPrice = $gProduct->getPrice();
                        $minimalPrice = (!$minimalPrice || $minimalPrice > $gPrice) ? $gPrice : $minimalPrice;
                        $maximalPrice += $gPrice;
                    }
                }

                $offer['@type'] = 'AggregateOffer';
                $offer['lowPrice'] = number_format($minimalPrice, 2, '.', '');
                $offer['highPrice'] = number_format($maximalPrice, 2, '.', '');
            } else {
                $offer['@type'] = 'Offer';
                $offer['price'] = number_format((float)$product->getFinalPrice(), 2, '.', '');
            }

            $data = array(
                '@context' => 'http://schema.org',
                '@type' => 'Product',
                'name' => $product->getName(),
                'image' => $product->getImageUrl(), // Google uses 1 image
                'description' => $info['description'], //use Desc if Shortdesc not work
                'sku' => $product->getSku(),
                'aggregateRating' => array(
                    '@type' => 'AggregateRating',
                    'bestRating' => '5',
                    'worstRating' => '0',
                    'ratingValue' => $info['ratingValue'],
                    'reviewCount' => $info['reviewCount']
                ),
                'offers' => $offer
            );

            return json_encode($data);
        }

        return null;
    }
}
