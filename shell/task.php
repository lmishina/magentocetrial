<?php
/**
 * Sort products by Ids provided in the txt file
 */

require_once 'abstract.php';

class My_Shell_Task extends Mage_Shell_Abstract
{
    const TASK_STATUS_ENABLED = 'enabled';
    const TASK_STATUS_DISABLED = 'disabled';

    const TASK_STATUS = 'status';
    const TASK_IMAGES = 'images';
    const TASK_CATEGORIES = 'categories';

    protected $_collection;

    protected $_status = array(
        self::TASK_STATUS => array(
            self::TASK_STATUS_ENABLED => array(),
            self::TASK_STATUS_DISABLED => array()
        )
    );

    protected $_images = array(
        self::TASK_IMAGES => array()
    );

    protected $_categories = array(
        self::TASK_CATEGORIES => array()
    );

    /**
     * Run script
     */
    public function run()
    {
        if ($path = $this->getArg('file')) {
            $productIds = $this->readFile($path);

            $limit = 100;
            $offset = 0;
            $rowCount = $this->_getProductCollection($productIds)->getSize();
            $i = $offset;

            if ($rowCount) {
                try {
                    do {
                        $collection = $this->_getProductCollection($productIds);
                        $collection->getSelect()->limit($limit, $offset);
                        $collection->load();

                        foreach ($collection as $product) {
                            $i++;

                            try {
                                $productId = $product->getId();

                                if ($product->getStatus() === Mage_Catalog_Model_Product_Status::STATUS_ENABLED) {
                                    $this->_status[self::TASK_STATUS][self::TASK_STATUS_ENABLED][] = $productId;
                                } else {
                                    $this->_status[self::TASK_STATUS][self::TASK_STATUS_DISABLED][] = $productId;
                                }

                                $categoryIds = $product->getCategoryIds() ? implode(',', $product->getCategoryIds()) : 0;
                                $this->_categories[self::TASK_CATEGORIES][$productId] = $categoryIds;

                                $imagesCount = Mage::getModel('catalog/product')->load($productId)
                                    ->getMediaGalleryImages()->getSize();
                                $this->_images[self::TASK_IMAGES][$productId] = $imagesCount;
                            } catch (Exception $e) {
                                $this->log('FAIL:' . $e->getMessage());
                            }
                        }
                        $offset += $limit;
                    } while ($offset < $rowCount);
                } catch (Exception $e) {
                    $this->log($e->getMessage());
                }

                foreach ($this->_status[self::TASK_STATUS] as $status => $statusArray) {
                    $statusArray = $statusArray ? implode(',', $statusArray) : 0;
                    $this->_output[self::TASK_STATUS][$status] = $statusArray;
                }

                print_r($this->_status);
                print_r($this->_images);
                print_r($this->_categories);
            } else {
                $this->log("Provided SKUs do not exist in database");
            }
        } else {
            echo $this->showHelp();
        }
    }

    /**
     * Retrieve file data as array
     *
     * @param   string $file
     * @return  array
     */
    public function readFile($file)
    {
        $ids = array();

        if (!is_dir($file)) {
            if (!file_exists($file)) {
                throw new Exception('File "' . $file . '" do not exists');
            }
        } else {
            throw new Exception($file . '" is not txt file');
        }

        $fileHandler = fopen($file, 'r');
        while ($rowData = fgets($fileHandler)) {
            $ids = array_merge($ids, explode(',', trim($rowData)));
        }
        fclose($fileHandler);

        if (!count($ids)) {
            throw new Exception('File doesn\'t contain product ids');
        }

        return $ids;
    }


    /**
     * Find duplicate and NULL SKUs
     *
     * @param array
     * @return Object
     */
    protected function _getProductCollection($skuIds)
    {
        if (!$this->_collection) {
            $collection = Mage::getResourceModel('catalog/product_collection');
            $collection->addFieldToFilter('entity_id', array("in" => array($skuIds)));

            $this->_collection = $collection;
        }

        return $this->_collection;
    }

    /**
     * Handle log info
     *
     * @param $str
     */
    public function log($str)
    {
        echo $str . PHP_EOL;
    }

    /**
     * Show Help Message
     *
     * @return string
     */
    public function showHelp()
    {
        return <<<USAGE
Usage: 
php task.php -- filename_to_import

Options:
  --file <path>            filename to import

USAGE;
    }
}

try {
    $shell = new My_Shell_Task();
    $shell->run();
} catch (Exception $e) {
    $shell->log($e->getMessage());
}